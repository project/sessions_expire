# INTRODUCTION
This module disables sessions GC and allows doing it via cron / drush command.

* Project page: https://drupal.org/project/sessions_expire

* To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/sessions_expire

# REQUIREMENTS
It requires only Drupal CORE.

# INSTALLATION
Install as any other contrib module, no specific installation step required 
for this module.

# CONFIGURATION
None as of now. 
