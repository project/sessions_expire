<?php

namespace Drupal\sessions_expire\Commands;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;

/**
 * Class SessionsExpireDrushCommands.
 *
 * @package Drupal\sessions_expire\Commands
 */
class SessionsExpireDrushCommands {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Session parameters.
   *
   * @var array|null
   */
  protected $parameters;

  /**
   * SessionsExpireDrushCommands constructor.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database service.
   * @param array|null $parameters
   *   Session parameters.
   */
  public function __construct(TimeInterface $time,
                              Connection $connection,
                              array $parameters = NULL) {
    $this->time = $time;
    $this->connection = $connection;
    $this->parameters = $parameters;
  }

  /**
   * Cleanup expired sessions.
   *
   * We do this in cron to ensure normal user requests are not delayed because
   * of GC activities.
   *
   * @command session-expire:gc
   *
   * @aliases sessions-gc
   */
  public function cleanupSessions() {
    $lifetime = $this->parameters['gc_maxlifetime'] ?? 2000000;
    $cookie_lifetime = $this->parameters['cookie_lifetime'] ?? 2000000;

    // There is a bug in default configuration, we try to workaround it here.
    // @see https://www.drupal.org/project/drupal/issues/2724065
    if ($lifetime < $cookie_lifetime) {
      $lifetime = $cookie_lifetime;
    }

    $this->connection->delete('sessions')
      ->condition('timestamp', $this->time->getCurrentTime() - $lifetime, '<')
      ->execute();
  }

}
