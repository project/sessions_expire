<?php

namespace Drupal\sessions_expire;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Class SessionsExpireServiceProvider.
 */
class SessionsExpireServiceProvider extends ServiceProviderBase implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    try {
      $session_storage_options = $container->getParameter('session.storage.options');

      // Match the value from core.services.yml.
      ini_set('session.gc_maxlifetime', $session_storage_options['gc_maxlifetime']);

      // Disable session garbage collection, we do it via cron job.
      // This is done to ensure user requests are served without any delays.
      ini_set('session.gc_probability', 0);
      $session_storage_options['gc_probability'] = 0;
      $container->setParameter('session.storage.options', $session_storage_options);
    } catch (\Exception $e) {
      // Do nothing, system might still be installing.
    }
  }

}
